function toggle(bx){
	var cbs = document.getElementsByTagName('input');
	for(var i=0; i < cbs.length; i++) {
		if(cbs[i].type == 'checkbox') {
			if(cbs[i].getAttribute('disabled') == null){
				cbs[i].checked = bx.checked;
			}
		}
	}	
}

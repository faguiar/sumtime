<?php 
App::import('Vendor','tcpdf/tcpdf'); 

$student = array();

//carrega o construtor do pdf
$Pdf = new TCPDF();

$Pdf->setPrintHeader(false);

$Pdf->setPrintFooter(false);

$Pdf->SetMargins(10, 20, 0);

$Pdf->AddPage('P','A4');

$student['name'] = $analysi['Analysi']['user_name'];
$student['course'] = $analysi['Course']['name'];

$html = "<html>
	<head><title>Certificado de Horas Extra-Curriculares</title></head>
	<body style=\"width:100%;\">
	<div style=\"text-align:center;\">
		<img src=\"{$this->webroot}app/webroot/images/pdf/UFPEL-ESCUDO-2013.png\"  width=\"60\" height=\"60\">
	</div>

	<div>
		<hr style=\"width:96%; margin-left:0 auto; margin-right: 0 auto; margin-bottom:20px;\">
	</div>

	<div style=\"text-align:center;\">
		<b>MINISTÉRIO DA EDUCAÇÃO</b> <br>
		<b>UNIVERSIDADE FEDERAL DE PELOTAS</b> <br>
		<b>CENTRO DE DESENVOLVIMENTO TECNOLÓGICO</b> <br>	    
	</div>

	<p>A coordenadoria do curso de {$student['course']}, atesta para devidos fins que o(a) docente {$student['name']},
	 graduando em {$student['course']},
	 realizou a carga horária de  mínima em atividades extra-curriculares. Sendo estas: </p>

	TABELA DE ATIVIDADES

	<div style=\"text-align:center;\">
	<br/>
		________________________________ <br/>
		Coordenador(a) de Atividades
	</div>

	</body>

</html>";


$Pdf->writeHTML($html);

echo $Pdf->Output('certificado.pdf','I'); 

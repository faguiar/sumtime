<div class="analysis form">
<?php echo $this->Form->create('Analysi', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo ('Criar Análise'); ?></legend>
	<?php
		echo $this->Form->input('user_id', array('type'=>'hidden'));
		echo $this->Form->input('file', array('type'=>'file', 'label' => 'Arquivo (.xml)'));
		echo $this->Form->input('status', array('type'=>'hidden'));
	?>
	</fieldset>
	<?php ?>
	<?php
		echo $this->Form->submit(
			'Salvar', 
			array('class' => 'btn btn-info btn-block', 'style' => 'width:75px;')
		);	
	?>
	<?php echo $this->Form->end(); ?>
</div>

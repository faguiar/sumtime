<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo 'sumTime' ?>
	</title>
	<?php
	
		echo $this -> Html -> meta('favicon');//
		echo $this -> Html -> css('base');
		echo $this -> Html -> css('header');
		echo $this -> Html -> css('comentarios');
		echo $this -> Html -> css('footer');
		echo $this -> Html -> css('bootstrap');//
		echo $this -> Html -> css('bootstrap-theme.min');//
		echo $this -> Html -> css('prettyPhoto');//
		echo $this -> Html -> script('bootstrap-dropdown');
		echo $this -> Html -> script('jquery.min');
		echo $this -> Html -> script('bootstrap');
		echo $this -> Html -> script('bootstrap.min');
		echo $this -> Html -> script('jquery-1.3.2.min');
		echo $this -> Html -> script('jquery-1.4.4.min');
		echo $this -> Html -> script('jquery-1.6.1.min'); 
		echo $this -> Html -> script('geral');
		
		echo $this -> fetch('meta');
		echo $this -> fetch('css');
		echo $this -> fetch('script');
		
	?>
	
</head>
<body>
	<div class="sumtime_Container">
		<div class="sumtime_Header">
			<div class="logo_container">
				<img src="<?php echo $this->webroot; ?>images/sumtime.png" style="height:35px !important; max-height:35px; margin-right:-10px;"/>
			</div>
			<div class="avatar_container">
				<?php if ($avatar): ?>
					<?php echo $this->Html->image('../'.$avatar, array('style'=>'width: 50px; height: 50px')); ?>
				<?php else: ?>
					<img src="<?php echo $this->webroot; ?>images/user_no_avatar.png"/>
				<?php endif; ?>
			</div>

			<div class="header_UserArea">
				<nav>
					<ul>
						<li>
							<a href="#"><?php echo $username ?> &#9660;</a>
							<ul>
								<?php if($this->Session->read('Auth.User.level') == 2):?>
									<?php if(isset($analysi['Analysi']['id'])): ?>
	
									<li><a href="<?php echo $this->Html->url(array('controller'=>'activities', 'action'=>'show', $analysi['Analysi']['id']));?>">Minha Análise</a></li>
									
									<?php else: ?>
									
									<li><a href="<?php echo $this->Html->url(array('controller'=>'analysis', 'action'=>'add'));?>">Criar Análise</a></li>
									<?php endif; ?>
								<?php endif; ?>
								<?php if($this->Session->read('Auth.User.level') == 3 || $this->Session->read('Auth.User.level') == 4):?>
									<li><a href="<?php echo $this->Html->url(array('controller'=>'analysis', 'action'=>'index'));?>">Lista de Análises</a></li>
								<?php endif; ?>
								<?php if($this->Session->read('Auth.User.level') == 5):?>
									<li><a href="<?php echo $this->Html->url(array('controller'=>'users', 'action'=>'index'));?>">Painel Administrativo</a></li>
								<?php endif; ?>
								<?php if($this->Session->read('Auth.User.level') < 5):?>
									<li><a href="<?php echo $this->Html->url(array('controller'=>'users', 'action'=>'edit', $this->Session->read('Auth.User.id')));?>">Editar Perfil</a></li>
								<?php endif; ?>
								<li><a href="<?php echo $this->Html->url(array('controller'=>'users', 'action'=>'logout'));?>">Logout</a></li>
							</ul>
						</li>
					</ul>
				</nav>
			</div>
		
			<div class="UserArea_dropdown">
			</div>
		</div>
		<div id="container">
			<div id="content">
				<?php echo $this->Session->flash(); ?>
				<?php echo $this->fetch('content'); ?>
			</div>
		</div>
	
		<div class="sumtime_Footer">
			<center>
				<br>
				<div class="logos">
					<a href="https://www.ufpel.edu.br"><img src="<?php echo $this->webroot; ?>images/logoufpel.png" style="height:120px !important; max-height:120px; margin-right:-10px;"/> </a>
					<a href="http://inf.ufpel.edu.br"><img src="<?php echo $this->webroot; ?>images/computacao.png" /> </a>
					<a href="http://cdtec.ufpel.edu.br/"><img src="<?php echo $this->webroot; ?>images/cdteclogo.png" style="height:50px" /> </a>
				</div>
				Computação - UFPel | (53) 3921 1327 | Campus Porto - UFPel, Rua Gomes Carneiro 1, 96010-610 | Pelotas-RS
			</center>
		</div>
	</div>

</body>

</html>

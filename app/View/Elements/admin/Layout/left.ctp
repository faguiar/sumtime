<div id="sidebar">
	<ul style="display: block;">
		<li class="<?php echo $layoutMenuActive['usuarios']; ?>">
			<?php echo $this->Html->link('<i class="icon icon-user"></i><span>Usuários</span>', array('controller' => 'users', 'action' => 'index'), array('escape'=>false)); ?>
		</li>
		<li class="<?php echo $layoutMenuActive['cursos']; ?>">
			<?php echo $this->Html->link('<i class="icon-list-alt"></i><span>Cursos</span>', array('controller' => 'courses', 'action' => 'index'), array('escape'=>false)); ?>
		</li>
		<li class="<?php echo $layoutMenuActive['modalidades']; ?>">
			<?php echo $this->Html->link('<i class="icon-list"></i><span>Modalidades</span>', array('controller' => 'modalities', 'action' => 'index'), array('escape'=>false)); ?>
		</li>
		<li class="<?php echo $layoutMenuActive['tipos']; ?>">
			<?php echo $this->Html->link('<i class="icon-align-justify"></i><span>Tipos</span>', array('controller' => 'types', 'action' => 'index'), array('escape'=>false)); ?>
		</li>
		<li class="<?php echo $layoutMenuActive['analises']; ?>">
			<?php echo $this->Html->link('<i class="icon-book"></i><span>Análises</span>', array('controller' => 'analysis', 'action' => 'index'), array('escape'=>false)); ?>
		</li>		
	</ul>
</div>

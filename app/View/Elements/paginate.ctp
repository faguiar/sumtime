<p><?php echo $this->Paginator->counter('Total registros: {:count}');?></p>

<ul class="pagination">
	<?php echo $this -> Paginator -> prev('‹‹', array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));?>
	<?php echo $this -> Paginator -> numbers(array(
									'separator' => '', 
									'class'=>'',
									'currentTag' => 'a',
									'currentClass' => 'active',
									'tag' => 'li')); ?>
	<?php echo $this -> Paginator -> next('››', array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));?>
</ul>



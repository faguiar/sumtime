<div class="alert alert-danger">
  <button type='button' class="close" data-dismiss="alert">
    <i>x</i>
  </button>
  <i class="icon-ok red"></i>
  <?php echo $message ?>
</div>

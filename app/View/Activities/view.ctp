<div class="activities view">
<h2><?php echo __('Activity'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($activity['Activity']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Analysi'); ?></dt>
		<dd>
			<?php echo $this->Html->link($activity['Analysi']['id'], array('controller' => 'analysis', 'action' => 'view', $activity['Analysi']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modality'); ?></dt>
		<dd>
			<?php echo $this->Html->link($activity['Modality']['name'], array('controller' => 'modalities', 'action' => 'view', $activity['Modality']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo $this->Html->link($activity['Type']['name'], array('controller' => 'types', 'action' => 'view', $activity['Type']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($activity['Activity']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Document'); ?></dt>
		<dd>
			<?php echo h($activity['Activity']['document']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Hours'); ?></dt>
		<dd>
			<?php echo h($activity['Activity']['hours']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Activity'), array('action' => 'edit', $activity['Activity']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Activity'), array('action' => 'delete', $activity['Activity']['id']), array(), __('Are you sure you want to delete # %s?', $activity['Activity']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Activities'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Activity'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Analysis'), array('controller' => 'analysis', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Analysi'), array('controller' => 'analysis', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Modalities'), array('controller' => 'modalities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Modality'), array('controller' => 'modalities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Types'), array('controller' => 'types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Type'), array('controller' => 'types', 'action' => 'add')); ?> </li>
	</ul>
</div>

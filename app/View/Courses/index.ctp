<?php echo $this -> Html -> link(__('Adicionar'), array('action' => 'add'), array('class'=>'btn btn-small btn-primary')); ?>
<br><br>
<?php if (sizeof($courses)==0) : echo 'Nenhum resultado foi encontrado.'; endif ?>
	
<?php if (sizeof($courses)>0) : ?>

<table class="table table-striped table-bordered table-hover">
	<thead>
		<th><?php echo $this->Paginator->sort('name', 'Nome'); ?></th>
		<th>Total Horas</th>
		<th></th>
	</thead>
	<tbody>
		<?php foreach ($courses as $course): ?>
			<tr>
				<td><?php echo h($course['Course']['name']); ?></td>
				<td><?php echo h($course['Course']['totalHours']); ?></td>
				<td class="actions">
					<?php echo $this->Html->link(__('Editar'), array('action' => 'edit',$course['Course']['id'])); ?>
					<?php echo $this->Form->postLink(__('Excluir'), array('action' => 'delete',$course['Course']['id']), null, __('Deseja excluir o registro #%s?', $course['Course']['name'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<?php echo $this->Element('admin/Layout/paginate');?>

<?php endif; ?>


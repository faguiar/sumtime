<?php
App::uses('AppController', 'Controller');
/**
 * Types Controller
 *
 * @property Type $Type 
 * @property PaginatorComponent $Paginator
 */
class TypesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function isAuthorized() {
		switch ($this->action) {
			case 'add' :
			case 'delete' :
			case 'edit' :
			case 'index' :
				if ($this->Auth->User('level') == 5) {
		     		return true;
		    		 break;
		       } else {
		     		return false;
		     		break;
		   	   }
	  	}
 	}

	public function index() {
		
		$arguments = array('order' => array(
								'Type.course_id'=> 'ASC'
							), 
							'limit' => Configure::read('PAGINATE_LIMIT_ADMIN'),
							'recursive'=>0
		);
	
		$this -> paginate = $arguments;
		$this->set('types', $this->Paginator->paginate());
		$courses = $this->Type->Modality->Course->find('list');
		$this->set('courses', $courses);
		$this->layout="admin";
		$this->setLayoutTitle('Tipos','Lista');
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Type->create();
			if ($this->Type->save($this->request->data)) {
				$this->Session->setFlash('A categoria foi salva corretamente.', 'flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('A categoria não pode ser salva. Por favor, tente novamente.', 'flash_error');
			}
		}
		
		$courses = $this->Type->Modality->Course->find('list');
		$this->set('courses', $courses);
		$this->layout="admin";
		$this->setLayoutTitle('Tipos','Adicionar');
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Type->exists($id)) {
			throw new NotFoundException('Categoria Inválida.', 'flash_error');
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Type->save($this->request->data)) {
				$this->Session->setFlash('A categoria foi salva corretamente.', 'flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('A categoria não pode ser salva. Por favor, tente novamente.', 'flash_error');
			}
		} else {
			$options = array('conditions' => array('Type.' . $this->Type->primaryKey => $id));
			$this->request->data = $this->Type->find('first', $options);
		}
		
		$courses = $this->Type->Modality->Course->find('list');
		$this->set('courses', $courses);
		$this->layout="admin";
		$this->setLayoutTitle('Tipos','Editar');
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Type->id = $id;
		if (!$this->Type->exists()) {
			throw new NotFoundException('Categoria Inválida.', 'flash_error');
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Type->delete()) {
			$this->Session->setFlash('A categoria foi removida corretamente.', 'flash_success');
		} else {
			$this->Session->setFlash('A categoria não pode ser salva. Por favor, tente novamente.', 'flash_error');
		}
		return $this->redirect(array('action' => 'index'));
	}
}

<?php
App::uses('AppController', 'Controller');
/**
 * Modalities Controller
 *
 * @property Modality $Modality 
 * @property PaginatorComponent $Paginator
 */
class ModalitiesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function isAuthorized() {
		switch ($this->action) {
			case 'add' :
			case 'delete' :
			case 'edit' :
			case 'index' :
			case 'ajaxGetModalities' :
				if ($this->Auth->User('level') == 5) {
		     		return true;
		    		 break;
		       } else {
		     		return false;
		     		break;
		   	   }
	  	}
 	}

	public function index() {
		$this->Modality->recursive = 0;
		$this->set('modalities', $this->Paginator->paginate());
		$this->layout="admin";
		$this->setLayoutTitle('Modalidades','Lista');
	}
/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Modality->create();
			if ($this->Modality->save($this->request->data)) {
				$this->Session->setFlash('A modalidade foi salva corretemante.', 'flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('A modalidade não pode ser salva. Por favor, tente novamente.', 'flash_error');
			}
		}
		
		$courses = $this->Modality->Course->find('list');
		$this->set('courses', $courses);
		$this->layout="admin";
		$this->setLayoutTitle('Modalidades','Adicionar');
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Modality->exists($id)) {
			throw new NotFoundException('Modalidade Inválida.', 'flash_error');
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Modality->save($this->request->data)) {
				$this->Session->setFlash('A modalidade foi salva corretemante.', 'flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('A modalidade não pode ser salva. Por favor, tente novamente.', 'flash_error');
			}
		} else {
			$options = array('conditions' => array('Modality.' . $this->Modality->primaryKey => $id));
			$this->request->data = $this->Modality->find('first', $options);
		}
		
		$courses = $this->Modality->Course->find('list');
		$this->set('courses', $courses);
		$this->layout="admin";
		$this->setLayoutTitle('Modalidades','Editar');
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Modality->id = $id;
		if (!$this->Modality->exists()) {
			throw new NotFoundException('Modalidade Inválida.', 'flash_error');
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Modality->delete()) {
			$this->Session->setFlash('A modalidade foi removida corretamente.', 'flash_success');
		} else {
			$this->Session->setFlash('A modalidade não pode ser removida. Por favor, tente novamente.', 'flash_error');
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function ajaxGetModalities(){
		$course_id = $this->request->data['course_id'];
		$modalities = $this->Modality->find('list', array('conditions' => array('Modality.course_id' => $course_id), 'recursive' => -1));
		header('Content-Type: application/json');
		die(json_encode($modalities));
	}
}

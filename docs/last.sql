-- phpMyAdmin SQL Dump
-- version 3.5.8.1deb1
-- http://www.phpmyadmin.net
--
-- Máquina: localhost
-- Data de Criação: 13-Jul-2014 às 23:50
-- Versão do servidor: 5.5.34-0ubuntu0.13.04.1
-- versão do PHP: 5.4.9-4ubuntu2.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `last`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `activities`
--

CREATE TABLE IF NOT EXISTS `activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `analysi_id` int(11) NOT NULL,
  `modality_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `hours` float NOT NULL,
  `document` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_activities_modalities1` (`modality_id`),
  KEY `fk_activities_types1` (`type_id`),
  KEY `fk_activities_analysis1` (`analysi_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=84 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `analysis`
--

CREATE TABLE IF NOT EXISTS `analysis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `file` varchar(255) NOT NULL,
  `selected` int(1) DEFAULT '0',
  `status` int(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_analysis_users1` (`user_id`),
  KEY `fk_analysis_courses1` (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `totalHours` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `courses`
--

INSERT INTO `courses` (`id`, `name`, `totalHours`) VALUES
(1, 'Ciencia da Computacao', 300),
(2, 'Engenharia da Computacao', 300);

-- --------------------------------------------------------

--
-- Estrutura da tabela `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `analysi_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_messages_analysis1` (`analysi_id`),
  KEY `fk_messages_users1` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `modalities`
--

CREATE TABLE IF NOT EXISTS `modalities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `hours` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_modalities_courses1` (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Extraindo dados da tabela `modalities`
--

INSERT INTO `modalities` (`id`, `course_id`, `name`, `hours`) VALUES
(1, 1, 'Ensino', 100),
(2, 1, 'Pesquisa', 100),
(3, 1, 'Extensao', 100),
(4, 2, 'Ensino', 100),
(5, 2, 'Pesquisa', 100),
(6, 2, 'Extensao', 100);

-- --------------------------------------------------------

--
-- Estrutura da tabela `types`
--

CREATE TABLE IF NOT EXISTS `types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `modality_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `hours` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_types_modalities1` (`modality_id`),
  KEY `fk_types_courses1` (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Extraindo dados da tabela `types`
--

INSERT INTO `types` (`id`, `course_id`, `modality_id`, `name`, `hours`) VALUES
(1, 1, 1, 'Monitoria', 51),
(2, 1, 3, 'Bolsa de Graduacao UFPel', 51),
(3, 1, 2, 'Iniciacao Cienti­fica', 51),
(4, 1, 3, 'Participacao em Extensao', 34),
(5, 1, 1, 'Semana Academica', 34),
(6, 1, 1, 'Cursos e Escolas', 51),
(7, 1, 2, 'Eventos Regionais', 17),
(8, 1, 2, 'Eventos Nacionais', 34),
(9, 1, 2, 'Eventos Internacionais', 34),
(10, 1, 2, 'Publicacao Artigo Regional', 34),
(11, 1, 2, 'Publicacao Artigo Nacional', 51),
(12, 1, 2, 'Publicacao Artigo Internacional', 68),
(13, 1, 3, 'Projetos Voluntarios', 51),
(14, 1, 1, 'Representacao Estudantil', 51),
(15, 1, 2, 'Obtencao de Premios', 68),
(16, 1, 1, 'Certificados Profissionais', 51),
(17, 1, 1, 'Disciplina Optativa', 68),
(18, 2, 4, 'Monitoria', 51),
(19, 2, 6, 'Bolsa de Graduacao UFPel', 51),
(20, 2, 5, 'Iniciacao Cientifica', 51),
(21, 2, 6, 'Participacao em Extensao', 34),
(22, 2, 4, 'Semana Academica', 34),
(23, 2, 4, 'Cursos e Escolas', 51),
(24, 2, 5, 'Eventos Regionais', 17),
(25, 2, 5, 'Eventos Nacionais', 34),
(26, 2, 5, 'Eventos Internacionais', 34),
(27, 2, 5, 'Publicacao Artigo Regional', 34),
(28, 2, 5, 'Publicacao Artigo Nacional', 51),
(29, 2, 5, 'Publicacao Artigo Internacional', 68),
(30, 2, 6, 'Projetos Voluntarios', 51),
(53, 2, 4, 'Representacao Estudantil', 51),
(54, 2, 5, 'Obtencao de Premios', 68),
(55, 2, 4, 'Certificados Profissionais', 51),
(56, 2, 6, 'Disciplina Optativa', 68);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `level` int(11) NOT NULL,
  `start` int(1) DEFAULT NULL,
  `registration` varchar(50) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(45) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_users_courses1` (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Extraindo dados da tabela `users`
-- usuario admin, senha admin

INSERT INTO `users` (`id`, `course_id`, `name`, `level`, `start`, `registration`, `image`, `username`, `password`, `created`, `modified`) VALUES
(1, NULL, 'Administrador', 5, NULL, NULL, NULL, 'admin', '2c1d448ccce9c4e387f4958c55ff563deb879ecd', '2014-07-13 18:54:59', '2014-07-13 18:54:59'),

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `activities`
--
ALTER TABLE `activities`
  ADD CONSTRAINT `fk_activities_analysis1` FOREIGN KEY (`analysi_id`) REFERENCES `analysis` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_activities_modalities1` FOREIGN KEY (`modality_id`) REFERENCES `modalities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_activities_types1` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `analysis`
--
ALTER TABLE `analysis`
  ADD CONSTRAINT `fk_analysis_courses1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_analysis_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `fk_messages_analysis1` FOREIGN KEY (`analysi_id`) REFERENCES `analysis` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_messages_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `modalities`
--
ALTER TABLE `modalities`
  ADD CONSTRAINT `fk_modalities_courses1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `types`
--
ALTER TABLE `types`
  ADD CONSTRAINT `fk_types_courses1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_types_modalities1` FOREIGN KEY (`modality_id`) REFERENCES `modalities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_courses1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
